A WARNING
=========
This is a public repository.

***DO NOT MAKE ANY COMMIT REFERENCING TPIN ACCOUNTS, DATA, OR INTERNAL INFRASTRUCTURE!***

If you are not a TPIN employee, intern, or contractor, you should *not* run the scripts hosted here. *Doing so may render your computer unusable!*

Computer Setup Scripts
======================
This repository contains scripts that TPIN staff can use to help configure their computer for work use. See below for per-operating system instructions.

Chrome OS
---------
Most Chrome OS settings will be applied automatically when you sign in with your TPIN Google account.

Note that Zoom Chat is not natively supported on Chromebooks. You can work around this in two ways (there is no harm in doing *both*):

* Install the Zoom app on your iPhone or Android smartphone for chat.
* Install the Zoom Linux app on your Chromebook.

The Zoom Linux app does not currently support Chromebook webcams or auto-start, but otherwise works well. It can be paired with [the Zoom PWA](https://pwa.zoom.us/wc) for meetings, webinars, and phone.

The `chromeos-linux-setup.sh` script will set up the Zoom Linux app for you on Chrome OS.

1. First, [turn on the Linux Environment](https://support.google.com/chromebook/answer/9145439) by opening up the **Settings** app, navigating to **Advanced > Developers > Linux development environment (Beta)**, and clicking **Turn on**. Follow the on-screen instructions, accepting the defaults.

2. At the end of the setup process, you should see a terminal window appear. Type in the following (or copy it and then paste into the terminal using **Ctrl + Shift + V**) and press **Enter**:

	```bash
	sudo apt -y update && curl https://bitbucket.org/tpin-it-security/computer-setup/raw/master/chromeos-linux-setup.sh | bash
	```

3. When the script finishes, you can close the terminal window by typing "exit" and pressing **Enter**. In the Chrome OS applications, you will now have a folder called "Linux apps" that contains two applications: One called "Terminal" (what you just used), and the "Zoom" Linux app. Open Zoom and [sign in](https://docs.google.com/document/d/1H9hWs6TnyFyrLBH8FsM9oRHzgIiByJrDwY3srSOL2sI/edit#bookmark=id.mth53igwm6uf)

If you prefer, there is also [a video walk-through](https://drive.google.com/file/d/1-V_YvViCwBrNARTDw_YXr7LD9CCOxNMt/view).

Note that the Zoom Linux app will not automatically start when you turn on your Chromebook, and can take a couple of minutes to come up first thing in the morning. You should continue to use [the Zoom PWA](https://pwa.zoom.us/wc) for meetings, webinars, and phone calls.
