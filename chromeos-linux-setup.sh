#!/usr/bin/env bash

# Create updater script.
#
cat << EOF > tpin-updater.sh
# Make sure that Debian is up-to-date.
#
apt -y update
apt -y full-upgrade
apt -y autoremove --purge --autoremove
apt -y clean

# Download and install the current version of Zoom.
#
BUILD_DIR="\$(mktemp -d)"
(
	cd "\$BUILD_DIR"
	curl -L -O https://zoom.us/client/latest/zoom_amd64.deb
	apt -y install ./zoom_amd64.deb
)
rm -rf "\$BUILD_DIR"
EOF

sudo mkdir -p /etc/cron.daily
sudo mv tpin-updater.sh /etc/cron.daily/tpin-updater.sh
sudo chmod 755 /etc/cron.daily/tpin-updater.sh

# Run updater script to initialize everything.
#
sudo /etc/cron.daily/tpin-updater.sh

# On some Chromebooks, Crostini's hardware acceleration causes SEVERE
# performance issues with Zoom. We use a custom .desktop file to force
# acceleration off as a work-around.
#
cat << EOF > Zoom.desktop
[Desktop Entry]
Name=Zoom
Comment=Zoom Video Conference
# Disable hardware acceleration to prevent excess CPU use.
Exec=/usr/bin/env LIBGL_ALWAYS_SOFTWARE=1 /usr/bin/zoom %U
Icon=Zoom
Terminal=false
Type=Application
Encoding=UTF-8
Categories=Network;Application;
StartupWMClass=zoom
MimeType=x-scheme-handler/zoommtg;x-scheme-handler/zoomus;x-scheme-handler/tel;x-scheme-handler/callto;x-scheme-handler/zoomphonecall;application/x-zoom
EOF

mkdir -p $HOME/.local/share/applications
mv Zoom.desktop $HOME/.local/share/applications/Zoom.desktop

# Exit message.
#
echo ""
echo "You can now exit the Terminal."
echo ""
echo "To do this, type \"exit\" (without the quotes) and press the \"Enter\" key."
